import data from '/mock.json';


let issue_id:number = 0;

// Main Info HTML Elements

let typeInput:HTMLElement|any = document.getElementById("info-type");
let priorityInput:HTMLElement|any = document.getElementById("info-priority");
let affectsVersionsInput:HTMLElement|any = document.getElementById("info-affects-versions");
let componentsInput:HTMLElement|any = document.getElementById("info-components");
let labelsInput:HTMLElement|any = document.getElementById("info-labels");
let sprintInput:HTMLElement|any = document.getElementById("info-sprint");
let storyPointInput:HTMLElement|any = document.getElementById("info-story-point");
let statusInput:HTMLElement|any = document.getElementById("info-status");
let resolutionInput:HTMLElement|any = document.getElementById("info-resolution");
let fixVersionsInput:HTMLElement|any = document.getElementById("info-fix-versions");

// Side Info HTML Elements

let assigneeInput:HTMLElement|any = document.getElementById("assignee-name");
let reporterInput:HTMLElement|any = document.getElementById("reporter-name");
let votesInput:HTMLElement|any = document.getElementById("side-info-votes");
let watchersInput:HTMLElement|any = document.getElementById("side-info-watchers");
let createdInput:HTMLElement|any = document.getElementById("side-info-created");
let updatedInput:HTMLElement|any = document.getElementById("side-info-updated");

// Setting respective values of HTML Elements

typeInput.innerHTML  = data.issues[issue_id].type;
priorityInput.innerHTML = data.issues[issue_id].priority;
affectsVersionsInput.innerHTML = data.issues[issue_id].affects_version;
componentsInput.innerHTML = data.issues[issue_id].components;
labelsInput.innerHTML = data.issues[issue_id].labels;
sprintInput.innerHTML = data.issues[issue_id].sprint;
storyPointInput.innerHTML = data.issues[issue_id].story_points;
statusInput.innerHTML = data.issues[issue_id].status;
resolutionInput.innerHTML = data.issues[issue_id].resolution;
fixVersionsInput.innerHTML = data.issues[issue_id].fix_version

console.log(assigneeInput);

assigneeInput.innerHTML = data.issues[issue_id].assignee;
console.log(data.issues[issue_id].assignee);

reporterInput.innerHTML = data.issues[issue_id].reporter;
votesInput.innerHTML = data.issues[issue_id].votes;
watchersInput.innerHTML = `<small>${data.issues[issue_id].watchers}</small>`;
createdInput.innerHTML = data.issues[issue_id].created_at;
updatedInput.innerHTML = data.issues[issue_id].updated_at;


