import data from "/mock.json";

// HTML Elements

const todoBox:HTMLElement|any= document.getElementById("todoBox");
const inProgressBox:HTMLElement|any = document.getElementById("inProgressBox");
const doneBox:HTMLElement|any = document.getElementById("doneBox");


// Status Tag Elements
const todoTag:HTMLElement|any= document.getElementById("status-tag-todo");
const inProgressTag:HTMLElement|any = document.getElementById("status-tag-in-progress");
const doneTag:HTMLElement|any = document.getElementById("status-tag-done");


// Color Variables
const highPriorityColor = "#CD0000";
const lowPriorityColor = "#D46D1A";
const inProgressColor:string = "#CDAF00";
const doneColor:string = "#75A701";


// Count variables
function countIssues():void{
  let todoCount:number = 0;
let inProgressCount:number = 0;
let doneCount = 0;

for(let i =0;i<data.issues.length;i++){
  if(data.issues[i].issue_status=='todo'){
    todoCount++;
  }
  else if(data.issues[i].issue_status=='in-progress'){
    inProgressCount++;
      
  }
  if(data.issues[i].issue_status=='done'){
    doneCount++;
  }
}
todoTag.innerText += ' ' + todoCount;
inProgressTag.innerText += ' ' +  inProgressCount;
doneTag.innerText += ' ' +  doneCount;

}




function showIssues():void{

    for(let i =0;i<data.issues.length;i++){
      let cardElement:HTMLElement|any = document.createElement("a");
      cardElement.className = "card-anchor";
      cardElement.href = "issue-details.html";
      let issueCard:HTMLElement|any = document.createElement("div");
      issueCard.className = "issue-card";
      cardElement.appendChild(issueCard);
      let issueCardRow1:HTMLElement|any = document.createElement("div");
      issueCardRow1.className = "issue-card-row1";
      issueCard.appendChild(issueCardRow1);

      let idTitle:HTMLElement|any = document.createElement("strong");
      idTitle.className = "main-id"
      idTitle.innerText = `ID: ${data.issues[i].id}`
      let dateTitle:HTMLElement|any = document.createElement("p");
      dateTitle.className = "date-title"
      dateTitle.innerText = `${data.issues[i].created_at}`

      issueCardRow1.appendChild(idTitle);
      issueCardRow1.appendChild(dateTitle);

      
      let issueCardRow2:HTMLElement|any = document.createElement("h3");
      issueCardRow2.className = "issue-card-row2";
      issueCardRow2.innerText = data.issues[i].title;
      issueCard.appendChild(issueCardRow2);

      let issueCardRow3:HTMLElement|any = document.createElement("p");
      issueCardRow3.className = "issue-card-row3";
      issueCardRow3.innerText = data.issues[i].description;
      issueCard.appendChild(issueCardRow3);

      let issueCardRow4:HTMLElement|any = document.createElement("div");
      issueCardRow4.className = "issue-card-row4";
      issueCard.appendChild(issueCardRow4);

      let assigneeProfileOuterBox:HTMLElement|any = document.createElement("div");
      assigneeProfileOuterBox.className = "assignee-profile-outer-box";
      issueCardRow4.appendChild(assigneeProfileOuterBox);
      

      let assigneeHeading:HTMLElement|any = document.createElement("p");
      assigneeHeading.className = "assignee-heading";
      assigneeHeading.innerText = "Assignee";
      assigneeProfileOuterBox.appendChild(assigneeHeading);

      let assigneeProfileInnerBox:HTMLElement|any = document.createElement("div");
      assigneeProfileInnerBox.className = "assignee-profile-inner-box";
      assigneeProfileOuterBox.appendChild(assigneeProfileInnerBox);

      let userCardImg:HTMLElement|any = document.createElement("img");
      userCardImg.src = data.issues[i].image_url;
    
      userCardImg.className = "user-card-img";
      assigneeProfileInnerBox.appendChild(userCardImg);

      let nameDesignationBox:HTMLElement|any = document.createElement("div");
      nameDesignationBox.className = "name-designation-box";
      assigneeProfileInnerBox.appendChild(nameDesignationBox);
      
      let assigneeName:HTMLElement|any = document.createElement("strong");
      assigneeName.innerText = data.issues[i].assignee
      assigneeName.className = "assignee-name-card"
      let assigneeDesignation:HTMLElement|any = document.createElement("small");
      assigneeDesignation.innerText = data.issues[i].assignee_designation;
      assigneeDesignation.className = "assignee-designation"
      nameDesignationBox.appendChild(assigneeName);
      nameDesignationBox.appendChild(assigneeDesignation);

      let issueCardRow4Right:HTMLElement|any = document.createElement("div");
      issueCardRow4Right.className = "issue-card-row4-right";
      issueCardRow4.appendChild(issueCardRow4Right);

      let statusTitle:HTMLElement|any = document.createElement("p");
      statusTitle.className = "status-title";
      statusTitle.innerText = "Status"
      let statusBox:HTMLElement|any = document.createElement("div");
      statusBox.className = "status-box";
      statusBox.id = `status-box-${i}`
      statusBox.innerText = data.issues[i].priority;
      issueCardRow4Right.appendChild(statusTitle);
      issueCardRow4Right.appendChild(statusBox);

      
        
        // Moving Issue Cards to their respective containers
      if(data.issues[i].issue_status=='todo'){
          todoBox.appendChild(cardElement)
      }
      else if(data.issues[i].issue_status=='in-progress'){
  
          inProgressBox.appendChild(cardElement);
      }
      if(data.issues[i].issue_status=='done'){

          doneBox.appendChild(cardElement);
      }

    //   Styling priority boxes according to their colors

      if(data.issues[i].priority == "High Priority"){
        let statusBox:HTMLElement|any = document.getElementById(`status-box-${i}`);
        statusBox.style.backgroundColor = highPriorityColor;
      }

      else if(data.issues[i].priority == "Low Priority"){
        let statusBox:HTMLElement|any = document.getElementById(`status-box-${i}`);
        statusBox.style.backgroundColor = lowPriorityColor;
      }
      else if(data.issues[i].priority == "In Progress"){
        let statusBox:HTMLElement|any = document.getElementById(`status-box-${i}`);
        statusBox.style.backgroundColor = inProgressColor;
      }
      else if(data.issues[i].priority == "Done"){
        let statusBox:HTMLElement|any = document.getElementById(`status-box-${i}`);
        statusBox.style.backgroundColor = doneColor;
      }
      
        
    }


            
}


countIssues();
showIssues();


// Issue Details PAge 

const mainInfoBox:HTMLAllCollection|any = document.getElementsByClassName("main-info-box");
console.log(mainInfoBox);

console.log("hiiiiii");


